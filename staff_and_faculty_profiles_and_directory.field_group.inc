<?php
/**
 * @file
 * staff_and_faculty_profiles_and_directory.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function staff_and_faculty_profiles_and_directory_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ldap|user|user|form';
  $field_group->group_name = 'group_ldap';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'LDAP',
    'weight' => '13',
    'children' => array(
      0 => 'ldap_user_puid_sid',
      1 => 'ldap_user_puid',
      2 => 'ldap_user_puid_property',
      3 => 'ldap_user_current_dn',
      4 => 'ldap_user_prov_entries',
      5 => 'ldap_user_last_checked',
      6 => 'ldap_user_ldap_exclude',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'LDAP',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-ldap field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_ldap|user|user|form'] = $field_group;

  return $export;
}
