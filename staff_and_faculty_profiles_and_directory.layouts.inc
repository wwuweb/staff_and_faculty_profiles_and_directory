<?php
/**
 * @file
 * staff_and_faculty_profiles_and_directory.layouts.inc
 */

/**
 * Implements hook_default_panels_layout().
 */
function staff_and_faculty_profiles_and_directory_default_panels_layout() {
  $export = array();

  $layout = new stdClass();
  $layout->disabled = FALSE; /* Edit this to true to make a default layout disabled initially */
  $layout->api_version = 1;
  $layout->name = '25_75';
  $layout->admin_title = '25/75';
  $layout->admin_description = '';
  $layout->category = 'Columns: 2';
  $layout->plugin = 'flexible';
  $layout->settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 1,
          1 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'left',
          1 => 'right',
        ),
        'parent' => 'main',
      ),
      'right' => array(
        'type' => 'region',
        'title' => 'Right',
        'width' => '74.9422976695704',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => 'right-column',
      ),
      'left' => array(
        'type' => 'region',
        'title' => 'Left',
        'width' => '25.057702330429603',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => 'left-column',
      ),
      1 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'top',
        ),
        'parent' => 'main',
        'class' => '',
      ),
      'top' => array(
        'type' => 'region',
        'title' => 'top',
        'width' => 100,
        'width_type' => '%',
        'parent' => '1',
        'class' => '',
      ),
    ),
  );
  $export['25_75'] = $layout;

  $layout = new stdClass();
  $layout->disabled = FALSE; /* Edit this to true to make a default layout disabled initially */
  $layout->api_version = 1;
  $layout->name = 'staff_profile';
  $layout->admin_title = 'Staff Profile';
  $layout->admin_description = '';
  $layout->category = '';
  $layout->plugin = 'flexible';
  $layout->settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 1,
          1 => 'main',
        ),
        'parent' => NULL,
      ),
      'main' => array(
        'type' => 'column',
        'width' => '49.92787287231314',
        'width_type' => '%',
        'children' => array(
          0 => 'main-row',
        ),
        'parent' => 'canvas',
        'class' => '',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'center',
        ),
        'parent' => 'main',
      ),
      'center' => array(
        'type' => 'region',
        'title' => 'User Info',
        'width' => 100,
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => 'user-info-individual',
      ),
      1 => array(
        'type' => 'column',
        'width' => '50.07212712768686',
        'width_type' => '%',
        'parent' => 'canvas',
        'children' => array(
          0 => 2,
        ),
        'class' => '',
      ),
      2 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'user_image',
        ),
        'parent' => '1',
        'class' => '',
      ),
      'user_image' => array(
        'type' => 'region',
        'title' => 'User Image',
        'width' => 100,
        'width_type' => '%',
        'parent' => '2',
        'class' => 'user-image-individual',
      ),
    ),
  );
  $export['staff_profile'] = $layout;

  return $export;
}
