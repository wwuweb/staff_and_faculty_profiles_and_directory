<?php
/**
 * @file
 * staff_and_faculty_profiles_and_directory.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function staff_and_faculty_profiles_and_directory_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ldap_servers" && $api == "ldap_servers") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "panels" && $api == "layouts") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function staff_and_faculty_profiles_and_directory_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
