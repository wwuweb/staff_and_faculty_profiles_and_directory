<?php
/**
 * @file
 * staff_and_faculty_profiles_and_directory.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function staff_and_faculty_profiles_and_directory_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'user_view_panel_context';
  $handler->task = 'user_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Staff and Faculty',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'user-individual-profile',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'flexible:staff_profile';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'user_image' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%user:field_professional_title %user:field_preferred_name %user:field-last-name';
  $display->uuid = '263722c2-60ec-48fe-bf12-b0cf493970a8';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-6519e722-352c-4386-952b-3b3b5b573d16';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'user:field_academic_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:user_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '6519e722-352c-4386-952b-3b3b5b573d16';
    $display->content['new-6519e722-352c-4386-952b-3b3b5b573d16'] = $pane;
    $display->panels['center'][0] = 'new-6519e722-352c-4386-952b-3b3b5b573d16';
    $pane = new stdClass();
    $pane->pid = 'new-e65a61e1-3d23-4e1a-9f03-e3f2218f97e6';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'user:field_office_hours';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:user_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'e65a61e1-3d23-4e1a-9f03-e3f2218f97e6';
    $display->content['new-e65a61e1-3d23-4e1a-9f03-e3f2218f97e6'] = $pane;
    $display->panels['center'][1] = 'new-e65a61e1-3d23-4e1a-9f03-e3f2218f97e6';
    $pane = new stdClass();
    $pane->pid = 'new-290065a5-1819-4bd4-892d-911a4548b049';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'user:field_phone_number';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'phone',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:user_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'default',
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '290065a5-1819-4bd4-892d-911a4548b049';
    $display->content['new-290065a5-1819-4bd4-892d-911a4548b049'] = $pane;
    $display->panels['center'][2] = 'new-290065a5-1819-4bd4-892d-911a4548b049';
    $pane = new stdClass();
    $pane->pid = 'new-8fa2b86b-258e-4aad-ab56-e63d72c30472';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'user:field_e_mail_address';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'email_contact',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:user_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '8fa2b86b-258e-4aad-ab56-e63d72c30472';
    $display->content['new-8fa2b86b-258e-4aad-ab56-e63d72c30472'] = $pane;
    $display->panels['center'][3] = 'new-8fa2b86b-258e-4aad-ab56-e63d72c30472';
    $pane = new stdClass();
    $pane->pid = 'new-a5f8077d-3e35-44e0-be60-9a8d7315135d';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'user:field_about';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:user_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = 'a5f8077d-3e35-44e0-be60-9a8d7315135d';
    $display->content['new-a5f8077d-3e35-44e0-be60-9a8d7315135d'] = $pane;
    $display->panels['center'][4] = 'new-a5f8077d-3e35-44e0-be60-9a8d7315135d';
    $pane = new stdClass();
    $pane->pid = 'new-74c1e387-0db5-4eef-8f9d-f1ab1ed6989a';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'user:field_year_started';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'inline',
      'formatter' => 'date_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'format_type' => 'short',
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_to' => '',
        'fromto' => 'both',
      ),
      'context' => 'argument_entity_id:user_1',
      'override_title' => 0,
      'override_title_text' => 'Year Started:',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = '74c1e387-0db5-4eef-8f9d-f1ab1ed6989a';
    $display->content['new-74c1e387-0db5-4eef-8f9d-f1ab1ed6989a'] = $pane;
    $display->panels['center'][5] = 'new-74c1e387-0db5-4eef-8f9d-f1ab1ed6989a';
    $pane = new stdClass();
    $pane->pid = 'new-60935dda-14cb-46ff-a2ec-04fab892b6c5';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'user:field_department_program';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'inline',
      'formatter' => 'hs_taxonomy_term_reference_hierarchical_text',
      'delta_limit' => '0',
      'delta_offset' => '0',
      'delta_reversed' => 0,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:user_1',
      'override_title' => 0,
      'override_title_text' => 'Department:',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 6;
    $pane->locks = array();
    $pane->uuid = '60935dda-14cb-46ff-a2ec-04fab892b6c5';
    $display->content['new-60935dda-14cb-46ff-a2ec-04fab892b6c5'] = $pane;
    $display->panels['center'][6] = 'new-60935dda-14cb-46ff-a2ec-04fab892b6c5';
    $pane = new stdClass();
    $pane->pid = 'new-315ee7eb-da00-4897-a86c-0eba68fb6328';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'user:field_tr_location';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'inline',
      'formatter' => 'hs_taxonomy_term_reference_hierarchical_text',
      'delta_limit' => '0',
      'delta_offset' => '0',
      'delta_reversed' => 0,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:user_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 7;
    $pane->locks = array();
    $pane->uuid = '315ee7eb-da00-4897-a86c-0eba68fb6328';
    $display->content['new-315ee7eb-da00-4897-a86c-0eba68fb6328'] = $pane;
    $display->panels['center'][7] = 'new-315ee7eb-da00-4897-a86c-0eba68fb6328';
    $pane = new stdClass();
    $pane->pid = 'new-366942aa-3cac-45f9-bae5-a76afa137232';
    $pane->panel = 'user_image';
    $pane->type = 'entity_field';
    $pane->subtype = 'user:field_your_photo';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'image',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'image_style' => 'medium',
        'image_link' => '',
      ),
      'context' => 'argument_entity_id:user_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '366942aa-3cac-45f9-bae5-a76afa137232';
    $display->content['new-366942aa-3cac-45f9-bae5-a76afa137232'] = $pane;
    $display->panels['user_image'][0] = 'new-366942aa-3cac-45f9-bae5-a76afa137232';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['user_view_panel_context'] = $handler;

  return $export;
}

/**
 * Implements hook_default_page_manager_pages().
 */
function staff_and_faculty_profiles_and_directory_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'department_directory';
  $page->task = 'page';
  $page->admin_title = 'Department Directory';
  $page->admin_description = 'This page will list the staff and faculty that are part of a specific department. ';
  $page->path = 'directory/%program';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array(
    'program' => array(
      'id' => 1,
      'identifier' => 'Department or Program',
      'name' => 'term',
      'settings' => array(
        'input_form' => 'term',
        'vids' => array(
          8 => '8',
          6 => 0,
          3 => 0,
          4 => 0,
          2 => 0,
          5 => 0,
          9 => 0,
          1 => 0,
          7 => 0,
          10 => 0,
        ),
        'breadcrumb' => 0,
        'transform' => 1,
      ),
    ),
  );
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_department_directory_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'department_directory';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Department Directory',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'flexible:25_75';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'right' => NULL,
      'left' => NULL,
      'top' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%program:name Directory';
  $display->uuid = '0675b1cc-d672-4951-882b-64d77b9b8b5d';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-d4fba101-9916-4630-94f1-5334bc406c08';
    $pane->panel = 'left';
    $pane->type = 'block';
    $pane->subtype = 'accordion_menu-1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'block',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'd4fba101-9916-4630-94f1-5334bc406c08';
    $display->content['new-d4fba101-9916-4630-94f1-5334bc406c08'] = $pane;
    $display->panels['left'][0] = 'new-d4fba101-9916-4630-94f1-5334bc406c08';
    $pane = new stdClass();
    $pane->pid = 'new-7394c8e6-3b8f-469b-83f7-bde151f9ab61';
    $pane->panel = 'right';
    $pane->type = 'views_panes';
    $pane->subtype = 'department_directory-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => array(
        0 => 'argument_term_1',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '7394c8e6-3b8f-469b-83f7-bde151f9ab61';
    $display->content['new-7394c8e6-3b8f-469b-83f7-bde151f9ab61'] = $pane;
    $display->panels['right'][0] = 'new-7394c8e6-3b8f-469b-83f7-bde151f9ab61';
    $pane = new stdClass();
    $pane->pid = 'new-f987f553-d78e-49d8-808d-aa10e4078291';
    $pane->panel = 'right';
    $pane->type = 'views_panes';
    $pane->subtype = 'basic_directory-panel_pane_4';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'path_visibility',
          'settings' => array(
            'visibility_setting' => '1',
            'paths' => 'directory/staff',
          ),
          'context' => 'empty',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'f987f553-d78e-49d8-808d-aa10e4078291';
    $display->content['new-f987f553-d78e-49d8-808d-aa10e4078291'] = $pane;
    $display->panels['right'][1] = 'new-f987f553-d78e-49d8-808d-aa10e4078291';
    $pane = new stdClass();
    $pane->pid = 'new-a552e69e-0195-4f45-801d-1320766a6e54';
    $pane->panel = 'right';
    $pane->type = 'views_panes';
    $pane->subtype = 'basic_directory-panel_pane_5';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'path_visibility',
          'settings' => array(
            'visibility_setting' => '1',
            'paths' => 'directory/faculty',
          ),
          'context' => 'empty',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'a552e69e-0195-4f45-801d-1320766a6e54';
    $display->content['new-a552e69e-0195-4f45-801d-1320766a6e54'] = $pane;
    $display->panels['right'][2] = 'new-a552e69e-0195-4f45-801d-1320766a6e54';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['department_directory'] = $page;

  return $pages;

}
